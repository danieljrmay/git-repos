<!-- markdownlint-disable MD033 -->
# <img src="doc/icon/icon.png" width="100" alt="git-repos icon"/> git-repos

[![pipeline status](https://gitlab.com/danieljrmay/git-repos/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/git-repos/commits/master)
[![Copr build status](https://copr.fedorainfracloud.org/coprs/danieljrmay/git-repos/package/git-repos/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/danieljrmay/git-repos/package/git-repos/)

## Introduction

The `git-repos` command line tool allows you to administer multiple
unrelated git repositories in a single command. It searches under the
specified directory (defaults to current working directory) for git
repositories and operates on them each in turn.

## Quick start

Say, like me, you keep a load of git repositories inside a bunch of
sub-directories inside `~/Projects`. You can get a quick overview of
the state of some of those repositories with the following command:

```bash-prompt
$> git-repos status ~/Projects/Bash
```

You might see an output like this:

![Typical output from `git-repos status ~/Projects/Bash`.](doc/screenshot/status.png)

You see a line (or multiple lines) for each repository path giving you
and indication of its state.

* **Untracked** indicates that the repository has untracked files. You
  might want to add them, delete them or append to them to a
  `.gitignore` file.
* **Unstaged** indicates that the repository has some changes to
  tracked files which have not been added (staged).
* **Uncommitted** indicates that the repository has added (staged)
  files which have not been committed.
* **Unpushed** indicates that the repository has commits which have
  not been pushed upstream.
* **Clean** indicates that the repository has no unstaged or
  uncommitted files and no unpushed commits.

If you are not interested in the *clean* repositories and just want to
see the repositories where some action is probably required then you
can use the `-q` or `--quiet` option:

```bash-prompt
$> git-repos -q status ~/Projects/Bash
```

In which case you would see:

![Typical output from `git-repos -q status ~/Projects/Bash`.](doc/screenshot/status-quiet.png)

If you know you might be offline for a while then it useful to be able
to quickly pull changes for every repository I have so I know that
I've got all the latest stuff downloaded. You can do that with:

```bash-prompt
$> git-repos pull-all ~/Projects
```

## Full Usage

```bash
git-repos [-d|--debug ] [-q|--quiet] [-v|--verbose] COMMAND [ DIRECTORY ]
```

### Options

#### `-d`, `--debug`

Print lots of debugging messages, probably only of interest to
developers.

#### `-q`, `--quiet`

Print minimal messages.

#### `-v`, `--verbose`

Print verbose messages about what is going on.

### Commands

#### `status`

Runs `git status` on each repository in turn and summerise their
status as follows:

* **Untracked** Repository has untracked files. You might want to add
   them, delete them or append to them to a `.gitignore` file.
* **Unstaged** Repository has modified tracked files which have not
  been added/staged.
* **Uncommitted** Repository has added/staged files which have not
  been committed.
* **Unpushed** Repository has commits which have not been pushed.
* **Clean** Repository has no unstaged or uncommitted files and no
  unpushed commits.

When the operating in quiet mode **Clean** repositories are omitted from
the output.

#### `tag-status`

Output whether the latest commit was tagged or not. This can be useful
when preparing releases.

#### `pull-all`

Execute a `git pull` on every git repository found. When the operating
in quiet mode repositories which are "Already up to date" are omitted
from the output.

#### `push-all`

Execute a `git push` on every git repository found. When the operating
in quiet mode repositories which are already "up to date" are omitted
from the output.

## Installation

### Copr Repository

If you use Fedora (or something compatible) then you can install
`git-repos` via a [Copr
repository](https://copr.fedorainfracloud.org/coprs/danieljrmay/git-repos/)
with:

```bash
dnf copr enable danieljrmay/git-repos
dnf install git-repos
```

## Alternatives

* [Gita](https://github.com/nosarthur/gita)
