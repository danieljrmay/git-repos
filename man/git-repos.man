.\" Process this file with the command:
.\" groff -m man -T utf8 git-repos.1 | gzip --best > git-repos.1.gz
.\" See:
.\" man groff_man
.\" for more information.
.TH GIT-REPOS 1 %ISO8601DATE% %VERSION%
.SH NAME
git-repos — Manage multiple git repositories.
.\"
.\"
.SH SYNOPSIS
.SY git-repos
.RB [ -d | --debug ]
.RB [ -q | --quiet ]
.RB [ -v | --verbose ]
.B  pull-all|push-all|status|tag-status
.RI [ DIRECTORY ]
.YS
.\"
.\"
.SH DESCRIPTION
The \fBgit-repos\fR command line tool allows you to administer
multiple git repositories in a single command. It searches under the
specified directory (defaults to current working directory) for git
repositories and operates on them each in turn.
.\"
.\"
.SH OPTIONS
The following options are supported.
.\"
.SS -d, --debug
Print lots of debugging messages, probably only of interest to
developers.
.\"
.SS -q, --quiet
Print the minimum of output messages.
.\"
.SS -v, --verbose
Print verbose messages about what is going on.
.\"
.\"
.SH COMMANDS
The following commands are supported.
.\"
.SS pull-all
Execute a \fBgit pull\fR on every git repository found. When the
operating in quiet mode repositories which are "Already up to date"
are omitted from the output.
.\"
.SS push-all
Execute a \fBgit push\fR on every git repository found. When the
operating in quiet mode repositories which are already "up to date"
are omitted from the output.
.\"
.SS status
Execute a \fBgit status\fR on every git repository found and summerise
the result. When the operating in quiet mode "Clean" repositories are
omitted from the output. The meaning of the summaries is as follows:
.TP
Untracked
.\"
Repository has untracked files. You might want to add them,
delete them or append to them to a \fB.gitignore\fR file.
.TP
Unstaged
.\"
Repository has tracked files have changes which have not been
added/staged.
.TP
Uncommitted
.\"
Repository has added/staged files which have not been committed.
.TP
Unpushed
.\"
Repository has commits which have not been pushed.
.TP
Clean
.\"
Repository has no unstaged or uncommitted files and no unpushed
commits.
.\"
.SS tag-status
Output whether the latest commit was tagged or not. This can be useful
when preparing releases.
.\"
.\"
.SH EXAMPLES
.\"
.SS Pulling
Pull repositories found under \fBmy-directory\fR with:
.EX
.B git-repos pull-all my-directory
.EE
.\"
.SS Status
Show the status of every repository found under the current working
directory with:
.EX
.B git-repos status
.EE
You can specify a directory other than the working directory with:
.EX
.B git-repos status /path/to/my-directory
.EE
.\"
.SH AUTHORS
.MT daniel.may@\:danieljrmay.com
Daniel J. R. May
.ME
