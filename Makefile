#!/usr/bin/make -f
#
# git-repos GNU Makefile
#
# See: https://www.gnu.org/software/make/manual
#
# Copyright (c) 2018 Daniel J. R. May
#

# Makefile command variables
BZIP2=/usr/bin/bzip2
DNF_INSTALL=/usr/bin/dnf --assumeyes install
GIT=/usr/bin/git
GZIP=/usr/bin/gzip
INSTALL=/usr/bin/install
INSTALL_DATA=$(INSTALL) --mode=644 -D
INSTALL_DIR=$(INSTALL) --directory
INSTALL_PROGRAM=$(INSTALL) --mode=755 -D
MOCK=/usr/bin/mock
PYINILINT=pyinilint
RPMLINT=/usr/bin/rpmlint
SED=/usr/bin/sed
SHELL=/bin/sh
SHELLCHECK=/usr/bin/shellcheck
SHELLCHECK_X=$(SHELLCHECK) -x
XZ=/usr/bin/xz

# Standard Makefile installation directories.
#
# The following are the standard GNU/RPM values which are defined in
# /usr/lib/rpm/macros. See
# http://www.gnu.org/software/make/manual/html_node/Directory-Variables.html
# for more information.
prefix=/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
datadir=$(prefix)/share
includedir=$(prefix)/include
infodir=$(prefix)/info
libdir=$(exec_prefix)/lib
libexecdir=$(exec_prefix)/libexec
localstatedir=$(prefix)/var
mandir=$(prefix)/man
rpmconfigdir=$(libdir)/rpm
sbindir=$(exec_prefix)/sbin
sharedstatedir=$(prefix)/com
sysconfdir=$(prefix)/etc

# Fedora installation directory overrides.
#
# We override some of the previous GNU/RPM default values with those
# values suiteable for a Fedora/RedHat/CentOS linux system, as defined
# in /usr/lib/rpm/redhat/macros.
#
# This section can be put into a separate file and included here if we
# want to create a more multi-platform Makefile system.
sysconfdir=/etc
defaultdocdir=/usr/share/doc
infodir=/usr/share/info
localstatedir=/var
mandir=/usr/share/man
sharedstatedir=$(localstatedir)/lib

# Makefile parameter variables
name:=git-repos
version:=$(shell awk '/Version:/ { print $$2 }' $(name).spec)
iso8601date:=$(shell date --iso-8601)
dist_name:=$(name)-$(version)
dist_tree-ish:=$(version)
tarball:=$(dist_name).tar.bz2
fedora_releasever:=$(shell python3 -c 'import dnf; db = dnf.dnf.Base(); print(db.conf.substitutions["releasever"])')
fedora_dist:=.fc$(fedora_releasever)
fedora_rpm_stem:=$(shell rpm --define='dist $(fedora_dist)' --specfile $(name).spec)
fedora_rpm:=$(fedora_rpm_stem).rpm
fedora_srpm:=$(patsubst %.noarch, %.src.rpm, $(fedora_rpm_stem))
fedora_mock_root:=fedora-$(fedora_releasever)-x86_64-kada-media
mock_resultdir=.
testdestdir:=testdestdir
requirements:=findutils git mock ncurses rpmlint ShellCheck

.PHONY: all
all: bash-completion/$(name) man/$(name).1.gz $(name)

$(name): $(name).bash
	$(SED) -e "s/%ISO8601DATE%/$(iso8601date)/g" -e "s/%VERSION%/$(version)/g" $< > $@

.PHONY: bash-completion
bash-completion: bash-completion/$(name)

bash-completion/$(name): bash-completion/$(name).bash
	$(SED) -e "s/%ISO8601DATE%/$(iso8601date)/g" -e "s/%VERSION%/$(version)/g" $< > $@

.PHONY: man
man: man/$(name).1.gz

man/$(name).1: man/$(name).man
	$(SED) -e "s/%ISO8601DATE%/$(iso8601date)/g" -e "s/%VERSION%/$(version)/g" $< > $@

man/$(name).1.gz: man/$(name).1
	$(GZIP) --best --stdout $< > $@

.PHONY: lint
lint:
	$(info lint:)
	$(SHELLCHECK_X) $(name).bash bash-completion/$(name).bash
	$(RPMLINT) $(name).spec

.PHONY: check
check:
	$(info check:)
	test -r $(name)
	test -r bash-completion/$(name)
	test -r man/$(name).1.gz

testdestdir:
	mkdir -p 	$(testdestdir)
	$(eval DESTDIR:=$(testdestdir))

.PHONY: testinstall
testinstall: | testdestdir install installcheck

.PHONY: install
install:
	$(info install:)
	$(INSTALL_PROGRAM) $(name) $(DESTDIR)$(bindir)/$(name)
	$(INSTALL_DATA) bash-completion/$(name) $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	$(INSTALL_DATA) fish-completion/$(name).fish $(DESTDIR)$(datadir)/fish/completions/$(name).fish
	$(INSTALL_DATA) man/$(name).1.gz $(DESTDIR)$(mandir)/man1/$(name).1.gz

.PHONY: installcheck
installcheck:
	$(info installcheck:)
	test -x $(DESTDIR)$(bindir)/$(name)
	test -r $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	test -r $(DESTDIR)$(datadir)/fish/completions/$(name).fish
	test -r $(DESTDIR)$(mandir)/man1/$(name).1.gz

.PHONY: uninstall
uninstall:
	$(info uninstall:)
	rm -f $(DESTDIR)$(bindir)/$(name)
	rm -f $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	rm -f $(DESTDIR)$(datadir)/fish/completions/$(name).fish
	rm -f $(DESTDIR)$(mandir)/man1/$(name).1.gz

.PHONY: testdist
testdist:
	$(info testdist:)
	mkdir $(dist_name) 
	cp --recursive --target-directory=$(dist_name) $(shell git ls-tree --name-only master)
	tar --create --bzip2 --file $(tarball) $(dist_name)
	rm -rf $(dist_name)

.PHONY: dist
dist:
	$(GIT) archive --format=tar --prefix=$(dist_name)/ $(dist_tree-ish) | $(BZIP2) > $(tarball)

.PHONY: srpm
srpm: $(tarball)
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --buildsrpm \
		--spec $(name).spec --sources $(tarball)

.PHONY: rpm
rpm: srpm
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --rebuild $(fedora_srpm)

.PHONY: clean
clean:
	$(info clean:)
	rm -f *.rpm *.bz2 bash-completion/$(name) man/$(name).1.gz 

.PHONY: distclean
distclean: clean
	$(info distclean:)
	find . -name *~ -delete
	rm -f $(name) *.log bash-completion/$(name) man/*.1
	rm -rf testdestdir

.PHONY: requirements
requirements:
	sudo $(DNF_INSTALL) $(requirements)

.PHONY: help
help:
	$(info help:)
	$(info Usage: make TARGET [VAR1=VALUE VAR2=VALUE])
	$(info )
	$(info Targets:)
	$(info   all             The default target, redirects to build.)
	$(info   lint            Lint the source files.)
	$(info   testinstall     Perform a test installation.)
	$(info   install         Install.)
	$(info   installcheck    Post-installation check of all installed files.)
	$(info   installgithooks Install the git hooks in the local repository.)
	$(info   dist		 Create a distribution tarball.)
	$(info   srpm		 Create a SRPM.)
	$(info   rpm		 Create a RPM.)
	$(info   clean           Clean up all generated binary files.)
	$(info   distclean       Clean up all generated files.)
	$(info   help            Display this help message.)
	$(info   printvars       Print variable values (useful for debugging).)
	$(info   printmakevars   Print the Make variable values (useful for debugging).)
	$(info )

.PHONY: printvars
printvars:
	$(info printvars:)
	$(info BZIP2=$(BZIP2))
	$(info DNF_INSTALL=$(DNF_INSTALL))
	$(info GIT=$(GIT))
	$(info INSTALL=$(INSTALL))
	$(info INSTALL_DATA=$(INSTALL_DATA))
	$(info INSTALL_DIR=$(INSTALL_DIR))
	$(info INSTALL_PROGRAM=$(INSTALL_PROGRAM))
	$(info MOCK=$(MOCK))
	$(info PYINILINT=$(PYINILINT))
	$(info RPMLINT=$(RPMLINT))
	$(info SHELL=$(SHELL))
	$(info SHELLCHECK=$(SHELLCHECK))
	$(info SHELLCHECK_X=$(SHELLCHECK_X))
	$(info prefix=$(prefix))
	$(info exec_prefix=$(exec_prefix))
	$(info bindir=$(bindir))
	$(info datadir=$(datadir))
	$(info includedir=$(includedir))
	$(info infodir=$(infodir))
	$(info libdir=$(libdir))
	$(info libexecdir=$(libexecdir))
	$(info localstatedir=$(localstatedir))
	$(info mandir=$(mandir))
	$(info rpmconfigdir=$(rpmconfigdir))
	$(info sbindir=$(sbindir))
	$(info sharedstatedir=$(sharedstatedir))
	$(info sysconfdir=$(sysconfdir))
	$(info defaultdocdir=$(defaultdocdir))
	$(info infodir=$(infodir))
	$(info localstatedir=$(localstatedir))
	$(info mandir=$(mandir))
	$(info sharedstatedir=$(sharedstatedir))
	$(info name=$(name))
	$(info dev_name=$(dev_name))
	$(info version=$(version))
	$(info dist_name=$(dist_name))
	$(info dist_tree-ish=$(dist_tree-ish))
	$(info tarball=$(tarball))
	$(info fedora_releasever=$(fedora_releasever))
	$(info fedora_dist=$(fedora_dist))
	$(info fedora_rpm_stem=$(fedora_rpm_stem))
	$(info fedora_rpm=$(fedora_rpm))
	$(info fedora_srpm=$(fedora_srpm))
	$(info fedora_mock_root=$(fedora_mock_root))
	$(info mock_resultdir=$(mock_resultdir))
	$(info testdestdir=$(testdestdir))
	$(info requirements=$(requirements))

.PHONY: printmakevars
printmakevars:
	$(info printmakevars:)
	$(info $(.VARIABLES))
