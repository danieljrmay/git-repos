#!/usr/bin/bash
# Bash Completion scriptlet for git-repos
#
# Author: Daniel J. R. May
# Version: %VERSION%
#

function _git-repos()
{
    # shellcheck disable=SC2034
    local cur prev words cword split
    _init_completion -s || return

    local opts='-d --debug -q --quiet -v --verbose'
    local cmds='pull-all push-all status tag-status'

    case "$prev" in
	git-repos)
	    mapfile -t COMPREPLY < <(compgen -W "$opts $cmds" -- "$cur")
	    return
	    ;;
	-d|--debug|-q|--quiet|-v|--verbose)
	    mapfile -t COMPREPLY < <(compgen -W "$cmds" -- "$cur")
	    return
	    ;;	    
	pull-all|push-all|status|tag-status)
	    _filedir -d
	    return
	    ;;
    esac
}
complete -F _git-repos git-repos
