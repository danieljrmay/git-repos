#
# Completions for the git-repos command
#

# All subcommmands of git-repos
set -l cmds pull-all push-all status tag-status

# Disable file completions for entire command, we only want
# directories
complete -c git-repos -f

# Subcommands
complete -c git-repos -n "not __fish_seen_subcommand_from $cmds" -a "pull-all"   -d "Pull all repos"
complete -c git-repos -n "not __fish_seen_subcommand_from $cmds" -a "push-all"   -d "Push all repos"
complete -c git-repos -n "not __fish_seen_subcommand_from $cmds" -a "status"     -d "Print repo status"
complete -c git-repos -n "not __fish_seen_subcommand_from $cmds" -a "tag-status" -d "Print repo tag status"

# Messaging switches
complete -c git-repos -s d -l debug   -d 'Print lots of debugging messages'
complete -c git-repos -s q -l quiet   -d 'Print minimum messages'
complete -c git-repos -s v -l verbose -d 'Print verbose messages'

# A directory must be specified after a subcommand
complete -c git-repos -n "__fish_seen_subcommand_from $cmds" -f -a "(__fish_complete_directories (commandline -ct))"
