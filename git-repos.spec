Name:           git-repos
Version:        0.12
Release:        1%{?dist}
Summary:        Tool to help administer multiple git repositories
License:        GPLv3
URL:            https://gitlab.com/danieljrmay/%{name}
Source0:        %{url}/-/archive/%{version}/%{name}-%{version}.tar.bz2
BuildArch:      noarch
BuildRequires:  make
Requires:       git, findutils, ncurses

%description

The git-repos tool can be used to perform operations on lots of
repositories at once. For instance "git-repos status DIRECTORY"
searches under a specified directory (defaults to current working
directory) for git repositories and then prints their status. This
makes it a useful tool for finding repositories which contain untracked
files or files which need to be commited.

%prep
%setup -q

%build
make

%install
%make_install

%files
%{_bindir}/git-repos
%{_datadir}/bash-completion/completions/git-repos
%{_datadir}/fish/completions/git-repos.fish
%{_mandir}/man1/git-repos.1.gz
%doc README.md
%license LICENSE

%changelog
* Fri Feb 18 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.12-1
- Add make to BuildRequires in spec.

* Tue Nov 17 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.11-1
- Add fish shell completions.

* Fri May 29 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.10-1
- Fix error message on missing command.

* Mon Oct  7 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.9-1
- Add output of last git tag on tag-status commmand.
- Fix 'Updated' messsage on pull-all command.

* Fri Aug 23 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.8-1
- Add verbosity options.

* Wed Aug 21 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.7-1
- Initial release on GitLab.
